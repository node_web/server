FROM node:10
WORKDIR /usr/local/node_web

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3000

CMD [ "node", "./bin/www" ]