var express = require('express');
var router = express.Router();
const ds = require('../../services/datastore')

router.get('/', (req, res) => {
    ds.users.selectAll({}).then(docs => {
        res.json(docs)
    }).catch(err => {
        res.status(500).json({message: err.message})
    })
})

router.get('/:_id', (req, res) => {
    ds.users.select(req.params._id).then(docs => {
        res.json(docs)
    }).catch(err => {
        res.status(500).json({message: err.message})
    })
})

router.post('/', (req, res) => {
    ds.users.insert(req.body).then(docs => {
        res.json(docs)
    }).catch(err => {
        res.status(500).json({message: err.message})
    })
})

router.put('/:_id', (req, res) => {
    ds.users.update(req.params._id, req.body).then(docs => {
        res.json(docs)
    }).catch(err => {
        res.status(500).json({message: err.message})
    })
})

router.delete('/:_id', (req, res) => {
    ds.users.delete(req.params._id).then(docs => {
        res.json(docs)
    }).catch(err => {
        res.status(500).json({message: err.message})
    })
})

module.exports = router