const Datastore = require('nedb')
const config = require('./config')
db = {}
db.users = new Datastore({ filename: `${config.database.location}/users.db`, autoload: true });

module.exports = {
    users:{
        selectAll(query){
            return new Promise((resolve, reject) => {
                db.users.find(query, (err, docs) => {
                    if(err){
                        return reject(err)
                    }
                    return resolve(docs)
                })
            })
        },
        select(_id){
            return new Promise((resolve, reject) => {
                db.users.findOne({_id}, (err, doc) => {
                    if (err) {
                        return reject(err)
                    }
                    return resolve(doc)
                })
            })
        },
        insert(object){
            return new Promise((resolve, reject) => {
                db.users.insert(object, (err, doc) => {
                    if (err) {
                        return reject(err)
                    }
                    return resolve(doc)
                })
            })
        },
        update(_id, object){
            return new Promise((resolve, reject) => {
                db.users.update({_id}, object, (err, doc) => {
                    if (err) {
                        return reject(err)
                    }
                    return resolve(doc)
                })
            })
        },
        delete(_id){
            return new Promise((resolve, reject) => {
                db.users.remove({_id}, (err, doc) => {
                    if (err) {
                        return reject(err)
                    }
                    return resolve(doc)
                })
            })
        }
    }
}
